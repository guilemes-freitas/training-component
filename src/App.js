import { Component } from "react";
import "./App.css";
import CharacterList from "./components/CharacterList";

class App extends Component {
  state = {
    characterList: [],
    nextUrl: "https://rickandmortyapi.com/api/character/"
  };
  getCharacters(url) {
    const { characterList } = this.state;
      fetch(url)
        .then((res) => res.json())
        .then((body) => {
          this.setState({
            // Fazendo o spread do estado atual e dos novos resultados e jogando tudo em novo array.
            characterList: [ ...characterList, ...body.results], 
            nextUrl: body.info.next
            // se não houver próxima página, o next será null
          })
        });
   }

  componentDidMount() {
    this.getCharacters(this.state.nextUrl)
  }

  componentDidUpdate(){
    const {nextUrl} = this.state;
    nextUrl ? this.getCharacters(nextUrl) : console.log("Over")
  }
  render() {
    const { characterList } = this.state;
    return (
      <div className="App">
        <header className="App-header">
          {characterList && <CharacterList list={characterList} />}
        </header>
      </div>
    );
  }
}

export default App;
