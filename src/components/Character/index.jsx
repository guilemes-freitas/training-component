import { Component } from "react";
import './style.css'
class Character extends Component {
  render() {
    const { image, status, origin } = this.props.char;
    return (
      <div className="card">
        <img className="icon" src={image} alt={this.props.name}></img>
        <h4>{this.props.name}</h4>
        <p>{status}</p>
        <p>{origin.name}</p>
      </div>
    );
  }
}

export default Character;
