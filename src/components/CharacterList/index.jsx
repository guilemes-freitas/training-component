import { Component } from "react";
import Character from "../Character";
import './style.css'

class CharacterList extends Component {
    render() {
        const { list } = this.props;
        return (
          <div>
            <div className="container">
                {
                    list.map((char) => <Character key={char.id} char={char} name={char.name}></Character>)
                }
            </div>
          </div>
        );
      }
}

export default CharacterList;
